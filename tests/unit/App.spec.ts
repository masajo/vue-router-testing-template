import { mount } from "@vue/test-utils";
import App from '@/App.vue';

// Routing imports
import { createRouter, createMemoryHistory, Router } from "vue-router";
import { routes } from "@/router";
import SettingsView from '@/views/SettingsView.vue';

describe('App.vue', () => {

    it('Renders a component in route', async () => {

        // CONTEXT
        // We define our router
        const router: Router = createRouter({
            history: createMemoryHistory(),
            routes: routes
        });

        // ACT:
        // We push a route to our router stack
        router.push('/settings');

        // Await for router ready
        await router.isReady();

        // Create Wrapper of App.vue
        const wrapper = mount(App, {
            global: {
                plugins: [router]
            }
        });

        // ASSERT:
        expect(wrapper.findComponent(SettingsView).exists()).toBe(true);


    });




})



